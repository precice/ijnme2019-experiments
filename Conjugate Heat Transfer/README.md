# Solve a conjugate heat transfer (CHT) problem using FEniCS and preCICE

The basic setup for this tutorial is described in the [preCICE wiki](https://github.com/precice/precice/wiki/Tutorial-for-solving-the-heat-equation-in-a-partitioned-fashion-using-FEniCS).

## Setups and results for the CHT case

* `qn_tuning`: comparison of different acceleration schemes for coupling.
  * `run`: setups to run the cases and raw results of the paper
    * `experiments_qn_sc`: use single value coupling with quasi-Newton
    * `experiments_rel_wi`: use waveform iteration with underrelaxation
    * `experiments_qn_wi`: use waveform iteration with full quasi-Newton
    * `experiments_rqn_wi`: use waveform iteration with reduced quasi-Newton
* `approximation_order_in_time`: experiments to determine the approximation order in time.
  * `g_pol`: experiments using a polynomial right-hand-side `g_pol`.
    * `run`: setups to run the cases and raw results of the paper
      * `experiments_ie`: use `g_pol` with `alpha = 1`, time stepping scheme implicit euler, linear interpolation in WI
      * `experiments_tr_linear`: use `g_pol` with `alpha = 1`, time stepping scheme trapezoidal rule, linear interpolation in WI
      * `experiments_tr_quadratic`: use `g_pol` with `alpha = 2`, time stepping scheme trapezoidal rule, quadratic interpolation in WI
      * `experiments_sdc`: use `g_pol` with `alpha = 3`, time stepping scheme SDC, cubic interpolation in WI
  * `g_tri`: experiments using trigonometric right-hand-side `g_tri`.
    * `run`: setups to run the cases and raw results of the paper
      * `experiments_ie_sc53`: use `g_tri` with time stepping scheme implicit euler and SC(5,3)
      * `experiments_ie_wi531`: use `g_tri` with time stepping scheme implicit euler and WI(5,3;1)
      * `experiments_tr_wi532`: use `g_tri` with time stepping scheme trapezoidal rule and WI(5,3;2)
      * `experiments_sdc_wi533`: use `g_tri` with time stepping scheme SDC and WI(5,3;3)
      * `experiments_tr_linear`: use `g_tri` with time stepping scheme trapezoidal rule and WI(x,y;1)
      * `experiments_tr_quadratic`: use `g_tri` with time stepping scheme trapezoidal rule and WI(x,y;2)

## Preparations

Depending on which setup you want to run, you have to use a different version of the fenics-adapter plus, if you want to apply waveform iteration, the waveform-bindings.

### Single Value Coupling

Uses the fenics-adapter and does not apply waveform iteration, but single value coupling. If you want to run this tutorial, you have to install the following specific version of the fenics-adapter:

* fenics-adapter: [`1ccab476e3923b34daf274e365ad777f9411569f`](https://github.com/precice/fenics-adapter/commit/1ccab476e3923b34daf274e365ad777f9411569f)

### Waveform Iteration with quasi Newton

This version of the tutorial uses the fenics-adapter and the waveform-bindings. If you want to run this tutorial, you have to install the following specific versions of the fenics-adapter and the waveform-bindings:

* fenics-adapter: [`61c8e2a613f47d79d03eb786f7446f691d9da2bb`](https://github.com/precice/fenics-adapter/commit/61c8e2a613f47d79d03eb786f7446f691d9da2bb)
* waveform-bindings: [`3dd475176b5b3540b3b91766957ef0394e816ec1`](https://github.com/BenjaminRueth/waveform-bindings/commit/3dd475176b5b3540b3b91766957ef0394e816ec1)

### Waveform Iteration with underrelaxation

This version of the tutorial uses the fenics-adapter and the waveform-bindings. If you want to run this tutorial, you have to install the following specific versions of the fenics-adapter and the waveform-bindings:

* fenics-adapter: [`61c8e2a613f47d79d03eb786f7446f691d9da2bb`](https://github.com/precice/fenics-adapter/commit/61c8e2a613f47d79d03eb786f7446f691d9da2bb)
* waveform-bindings: [`ca8e5e8786a9c76552a7d203b2660213a6e5add5`](https://github.com/BenjaminRueth/waveform-bindings/commit/ca8e5e8786a9c76552a7d203b2660213a6e5add5)

## Running

The solvers `heat.py` and `heat_subcycling.py` provide a wide range of command line options to specify all different parts of our simulation pipeline, such as time-stepping, acceleration scheme or right-hand-side. The `--help` option provides more details.

### Experiments & Configuration files

Inside the `run` folder of each setup you can find folders called `experiments*`, where the required config files for each experiment have to be placed. Our experiments are structured in a hierarchical fashion: 

1. `experiments*/` holds all experiments of a certain type
2. `WRxy/` holds all experiments where a multirate setup with `x` substeps for the first and `y` substeps for the second solver are used
3. `dTx.x/` holds all experiments with a window size of `x.x`
4. `SERIAL_FIRST_DIRICHLET/` we only consider experiments, with serial coupling, where the Dirichlet solver comes first. If you want, you can also provide other coupling schemes supported by preCICE via the option `--coupling-scheme`. However, this is out-of-scope for this paper.

At the end of this hierarchy, the following configuration files have to be provided:

* `precice-adapter-config-D.json`: adapter configuration for the Dirichlet solver
* `precice-adapter-config-N.json`: adapter configuration for the Neumann solver
* `precice-config.xml`: preCICE configuration file
* (optional) `runall.sh`: commands necessary for running the case. Can be used to automatize running of several cases. If you want to use this script, you have to add the correct prefix (`--experiments-prefix`) or rename the root folder to `experiments`.

### Example: WI(1,1;1) using QN-WI

Make sure to have the software required for Waveform Iteration installed. For running the basic case **WI(1,1;1)** using **QN-WI** everything is already provided in this repository. Just run the following commands in two independent terminals from the `run` folder:

```
python3 heat.py -d
```
and
```
python3 heat.py -n
```

We do not specificfy other parameters like the time stepping scheme being used, the interpolation scheme or the multirate setup. Therefore the default parameters are used and we use an Implicit Euler time stepping scheme for both participants, a multirate setup with 1 step on each side and linear interpolation between substeps. 

If you want to run a certain kind of experiment, for example you want to reproduce the experiments from `experiments_ie`, use `--experiments-prefix` to specify the folder where the solver should look for the configuration files. By default the folder `experiments` is used.
