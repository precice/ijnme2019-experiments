python3 ../../../../{{executable}} -d --monolithic -dT {{ window_size }} -tol {{error_tolerance}} -mth {{method}} -t {{time_dependence}} -T {{simulation_time}}
