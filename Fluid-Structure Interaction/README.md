# Setups and results for FSI case

* `physics`: comparison between Nutils-FEniCS and OpenFOAM-FEniCS for QN-SC
  * `run-sc`: setups to run the cases
  * `results`: raw results of the paper
* `convergence`: comparing different timestep sizes and different multi-rate settings for QN-WI
  * `run-wi`: setups to run the cases
  * `results`: raw results of the paper 
