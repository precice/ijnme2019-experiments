# Convergence results for FSI case

Please adjust and run `test_interval.py` to compare results.

## Multi-rate setting

* case 17: MR11
* case 18: MR23
* case 19: MR32

## Time step size

* A: dt = 0.0025
* B: dt = 0.00125
* C: dt = 0.000625
* D: dt = 0.0003125
* E: dt = 0.00015625
* F: dt = 7.8125E-05
* G: dt = 3.90625E-05

