import numpy as np

#MR23
#start = 449
#end = 555

#MR32
start = 341
end = 447

# MR11
#start = 233
#end = 339


testseries = 19

testcases = ['A', 'B', 'C', 'D', 'E']
#testcases = ['A', 'B', 'C', 'D', 'E','F']
n_testcases = len(testcases)
displacements = {testcase:[] for testcase in testcases}
displacements['Ref'] = []
velocities = []

coarsest_resolution = 4
end_timestep = coarsest_resolution

# load experiments
for testcase in testcases:
  for timestep in range(1, end_timestep+1):
    filename = testcase + str(testseries) + '/SOLIDZ_Mesh-SOLIDZ.dt' + str(timestep) + '.vtk'
    with open(filename, 'r') as f:
      l = [line.strip().split(' ') for line in f]
    displacements[testcase].append(np.asarray(l[start:end]).ravel().astype(np.float))
  end_timestep = end_timestep * 2
  
# load reference, G17
for timestep in range(1, 257):
  filename = 'G' + str(17) + '/SOLIDZ_Mesh-SOLIDZ.dt' + str(timestep) + '.vtk'
  with open(filename, 'r') as f:
    l = [line.strip().split(' ') for line in f]
  displacements['Ref'].append(np.asarray(l[233:339]).ravel().astype(np.float))

# compute errors of complete time interval
errors = []
end_timestep = coarsest_resolution
for testcase in testcases:
  err = 0
  resolution_diff = int(2**6 * coarsest_resolution / end_timestep)
  for timestep in range(1, end_timestep+1):
    err = err + np.linalg.norm(displacements[testcase][timestep-1]-displacements['Ref'][timestep*resolution_diff-1]) **2
  err = err / end_timestep
  err = np.sqrt(err)
  print(err)
  errors.append(err)
  end_timestep = end_timestep * 2

# compute convergence factors for quick feedback  
factors = []
for i in range(0,n_testcases-1):
  factors.append(errors[i]/errors[i+1])

print(factors)  
