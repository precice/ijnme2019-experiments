from fenics import *
from fenics import Constant, Function, AutoSubDomain, RectangleMesh, VectorFunctionSpace, interpolate, \
TrialFunction, TestFunction, Point, Expression, DirichletBC, nabla_grad,\
Identity, inner,dx, ds, sym, grad, lhs, rhs, File, solve

from ufl import nabla_div
from fenicsadapter import Adapter


# define the two kinds of boundary: clamped and coupling Neumann Boundary
def is_inside_clamped_boundary(x, on_boundary):
    return on_boundary and abs(x[1])<tol


def is_inside_neumann_boundary(x, on_boundary):
    """
    determines whether a node is on the coupling boundary
    
    """
    return on_boundary and ((abs(x[1]-1)<tol) or abs(abs(x[0])-W/2)<tol)


# Geometry and material properties
dim = 2  # number of dimensions
H = 1
W = 0.1
rho = 3000
E = 4000000.0
nu = 0.3

mu = Constant(E / (2.0*(1.0 + nu)))

lambda_ = Constant(E*nu / ((1.0 + nu)*(1.0 - 2.0*nu)))

# create Mesh
n_x_Direction = 5
n_y_Direction = 50
mesh = RectangleMesh(Point(-W/2, 0), Point(W/2, H), n_x_Direction, n_y_Direction)

h = Constant(H/n_y_Direction)


# create Function Space
V = VectorFunctionSpace(mesh, 'P', 2)

tol = 1E-14

# Trial and Test Functions
du = TrialFunction(V)
v = TestFunction(V)

# updated fields
u_np1 = Function(V)
v_np1 = Function(V)
a_np1 = Function(V)

# function known from previous timestep
u_n = Function(V)
v_n = Function(V)
a_n = Function(V)


# initial value for force and displacement field
f_N_function = interpolate(Expression(("0","0"), degree=1), V)
u_function = interpolate(Expression(("0","0"), degree=1), V)

# define coupling boundary
coupling_boundary = AutoSubDomain(is_inside_neumann_boundary)

# get the adapter ready

# read fenics-adapter json-config-file)

adapter_config_filename = "precice-adapter-config-fsi-s.json"
wr_interpolation_strategy= "linear"
           
precice = Adapter(adapter_config_filename, wr_interpolation_strategy=wr_interpolation_strategy)

clamped_boundary_domain=AutoSubDomain(is_inside_clamped_boundary)
precice_dt = precice.initialize(coupling_subdomain=coupling_boundary,
                                mesh=mesh,
                                read_field=f_N_function,
                                write_field=u_function,
                                u_n=(u_n, v_n, a_n),
                                dimension=dim,
                                dirichlet_boundary=clamped_boundary_domain)

dt = Constant(precice_dt)
force_boundary = AutoSubDomain(is_inside_neumann_boundary)

# clamp the beam at the bottom
bc = DirichletBC(V, Constant((0, 0)), is_inside_clamped_boundary)

# alpha method parameters
# alpha_m = alpha_f = 0 implements newmark method
alpha_m = Constant(0.0)
alpha_f = Constant(0.0)
# alpha_m = 0.2, alpha_f = 0.4 implements generalized alpha method
# alpha_m = Constant(0.2)
# alpha_f = Constant(0.4)
gamma = Constant(0.5+alpha_f-alpha_m)
beta = Constant((gamma+0.5)**2/4.)


# Define strain
def epsilon(u):
    return 0.5*(nabla_grad(u) + nabla_grad(u).T)


# Define Stress tensor
def sigma(u):
    return lambda_*nabla_div(u)*Identity(dim) + 2*mu*epsilon(u)


# Define Mass form
def m(u, v):
    return rho*inner(u,v)*dx


# Elastic stiffness form
def k(u, v):
    return inner(sigma(u), sym(grad(v))) * dx


# functions for updating velocity and acceleration
def update_acceleration(u, u_old, v_old, a_old, ufl=True):
    if ufl:
        dt_ = dt
        beta_ = beta
    else:
        dt_ = float(dt)
        beta_ = float(beta)
    
    return ((u - u_old - dt_*v_old)/beta/dt_**2 
            - (1-2*beta_)/2/beta_*a_old)


def update_velocity(a, _, v_old, a_old, ufl=True):
    if ufl:
        dt_ = dt
        gamma_ = gamma
    else:
        dt_ = float(dt)
        gamma_ = float(gamma)
    
    return v_old + dt_*((1-gamma_)*a_old + gamma_*a)


def avg(x_old, x_new, alpha):
    return alpha*x_old + (1-alpha)*x_new


# acceleration at t_n+1
da = update_acceleration(du, u_n, v_n, a_n, ufl=True)
# residual
res = m(avg(a_n, da, alpha_m), v) + k(avg(u_n, du, alpha_f), v)

Forces_x, Forces_y = precice.create_force_boundary_condition(V)

a_form = lhs(res)
L_form = rhs(res)

# Prepare for time-stepping

# parameters for Time-Stepping
t = 0.0
n = 0
time = []
u_tip = []
time.append(0.0)
u_tip.append(0.0)
E_ext = 0

displacement_out = File("Solid/FSI-S/u_fsi.pvd")
u_n.rename("Displacement", "")
u_np1.rename("Displacement", "")
displacement_out << u_n

# time loop for coupling
while precice.is_coupling_ongoing():

    A, b = assemble_system(a_form, L_form, bc)

    b_forces = b.copy() # b is the same for every iteration, only forces change
    
    for ps in Forces_x:
        ps.apply(b_forces)
    for ps in Forces_y:
        ps.apply(b_forces)
        
    assert(b is not b_forces)
    print("CSM: solving structure")
    solve(A, u_np1.vector(), b_forces)
    a_np1 = project(update_acceleration(u_np1, u_n, v_n, a_n, ufl=False), V)
    v_np1 = project(update_velocity(a_np1, u_n, v_n, a_n, ufl=False), V)
    
    print("CSM: advancing preCICE")
    t, n, precice_timestep_complete, precice_dt, Forces_x, Forces_y = precice.advance(write_function=u_np1,
                                                                                      u_np1=(u_np1, v_np1, a_np1),
                                                                                      u_n=(u_n, v_n, a_n),
                                                                                      t=t,
                                                                                      dt=float(dt),
                                                                                      n=n)
    
    if precice_timestep_complete:
    
        print("CSM: time window complete")
        
        # if n % 10==0:
        displacement_out << (u_n,t)
    
        u_tip.append(u_n(0.,1.)[0])
        time.append(t)
    
precice.finalize()
