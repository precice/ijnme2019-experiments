# Running

run coupled simulation by opening two terminals:

* go to `run-wi/FEniCS`, from there run `python3 nsale.py`
* go to `run-wi/Nutils`, from there run `python3 perp-flap.py`

or directly run a complete set of simulations with `run-convergence`

