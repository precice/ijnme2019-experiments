cp configs/precice-config-MR32-A.xml precice-config.xml
cd FEniCS
python3 perp-flap.py &
cd ../Nutils
python3 nsale.py 
cd ..
cp FEniCS/precice-SOLIDZ-* FEniCS/SOLIDZ_Mesh-SOLIDZ* ../results/A19
./Allclean
cp configs/precice-config-MR32-B.xml precice-config.xml
cd FEniCS
python3 perp-flap.py &
cd ../Nutils
python3 nsale.py
cd ..
cp FEniCS/precice-SOLIDZ-* FEniCS/SOLIDZ_Mesh-SOLIDZ* ../results/B19
./Allclean
cp configs/precice-config-MR32-C.xml precice-config.xml
cd FEniCS
python3 perp-flap.py &
cd ../Nutils
python3 nsale.py
cd ..
cp FEniCS/precice-SOLIDZ-* FEniCS/SOLIDZ_Mesh-SOLIDZ* ../results/C19
./Allclean
cp configs/precice-config-MR32-D.xml precice-config.xml
cd FEniCS
python3 perp-flap.py &
cd ../Nutils
python3 nsale.py
cd ..
cp FEniCS/precice-SOLIDZ-* FEniCS/SOLIDZ_Mesh-SOLIDZ* ../results/D19
./Allclean
cp configs/precice-config-MR32-E.xml precice-config.xml
cd FEniCS
python3 perp-flap.py &
cd ../Nutils
python3 nsale.py
cd ..
cp FEniCS/precice-SOLIDZ-* FEniCS/SOLIDZ_Mesh-SOLIDZ* ../results/E19
./Allclean
cp configs/precice-config-MR32-F.xml precice-config.xml
cd FEniCS
python3 perp-flap.py &
cd ../Nutils
python3 nsale.py
cd ..
cp FEniCS/precice-SOLIDZ-* FEniCS/SOLIDZ_Mesh-SOLIDZ* ../results/F19
./Allclean
