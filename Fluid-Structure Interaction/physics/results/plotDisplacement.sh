#! /bin/bash
gnuplot -p << EOF
set grid
set xlabel 'Time [s]' font ",12" offset -3
set ylabel 'X-Displacement [m]' font ",12" offset 0

set key right top 
#set key at graph 1.0, 0.2
set encoding utf8
set xtics font ",12" 
set ytics font ",12" 
set key font ",14" 
##set lmargin 15


#set xrange [0:0.08]
#set yrange [-0.01:0.21]


plot "OpenFOAM-FEniCS/precice-fenics-watchpoint-Tip.log" using 1:8 with lines dt 1 lc rgb "dark-grey" lw 3 title 'OpenFOAM-FEniCS', \
     "Nutils-FEniCS/precice-SOLIDZ-watchpoint-Tip.log" using 1:4 with lines dt 3 lc rgb "black" lw 3 title 'Nutils-FEniCS'
EOF

