# Perpendicular Flap with Nutils and FEniCS

* no waveform iteration, just basic preCICE subcycling

## Start

* `cd Nutils` and `python3 nsale.py`
* `cd FEniCS` and `python3 perp_flap.py`

## Postprocess

* Nutils: anywhere, do `treelog-fetch ~/public_html/nsale.py/log-XXX.html '*.vtk'`


