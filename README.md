# Numerical Experiments: Quasi-Newton Waveform Iteration for Partitioned Fluid-Structure Interaction

This repository contains the code and instructions for running the examples of the paper *Quasi-Newton Waveform Iteration for Partitioned Fluid-Structure Interaction* by Benjamin Rüth, Benjamin Uekermann, Miriam Mehl, Philipp Birken, Azahar Monge and Hans-Joachim Bungartz.

## Conjugate Heat Transfer

An academic test case to check

1. How well does Quasi-Newton converge for Waveform Iteration?
2. Can we recover high order in time using Waveform Iteration?

## Fluid-Structure Interaction

This test case generalizes our observations from the first test case to a more involved fluid-structure interaction problem.
